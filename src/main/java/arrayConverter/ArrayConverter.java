package arrayConverter;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Nikita_K on 24.05.2017.
 */
public class ArrayConverter {

    public static <T> Collection<T> convertToCollection(T[] source, Collection<T> target) {
        target.addAll(Arrays.asList(source));
//        Collections.addAll(target, source);
        return target;
    }

}
