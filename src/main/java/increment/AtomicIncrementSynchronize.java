package increment;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Nikita_K on 24.05.2017.
 */
public class AtomicIncrementSynchronize implements IncrementSynchronize {

    private final AtomicInteger atomicValue;

    public AtomicIncrementSynchronize() {
        atomicValue = new AtomicInteger(0);
    }

    public int getNextValue() {
        return atomicValue.getAndIncrement();
    }

    @Override
    public int getCurrentValue() {
        return atomicValue.intValue();
    }

}
