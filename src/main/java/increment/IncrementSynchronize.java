package increment;

/**
 * Created by Nikita_K on 24.05.2017.
 */
public interface IncrementSynchronize {

    int getNextValue();

    int getCurrentValue();

}
