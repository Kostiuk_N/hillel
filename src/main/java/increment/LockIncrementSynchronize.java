package increment;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Nikita_K on 24.05.2017.
 */
public class LockIncrementSynchronize implements IncrementSynchronize {

    private final    Lock rLock;
    private volatile int  value;

    public LockIncrementSynchronize() {
        rLock = new ReentrantLock();
    }

    public int getNextValue() {
        try {
            rLock.lock();
            return value++;
        } finally {
            rLock.unlock();
        }
    }

    @Override
    public int getCurrentValue() {
        return value;
    }


}
