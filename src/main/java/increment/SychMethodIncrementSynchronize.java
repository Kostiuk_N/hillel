package increment;

/**
 * Created by Nikita_K on 24.05.2017.
 */
public class SychMethodIncrementSynchronize implements IncrementSynchronize {

    private volatile int value;

    public SychMethodIncrementSynchronize() {}

    public synchronized int getNextValue() {
        return value++;
    }

    @Override
    public int getCurrentValue() {
        return value;
    }


}
