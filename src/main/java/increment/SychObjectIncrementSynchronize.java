package increment;

/**
 * Created by Nikita_K on 24.05.2017.
 */
public class SychObjectIncrementSynchronize implements IncrementSynchronize {

    private final    Object lock;
    private volatile int    value;

    public SychObjectIncrementSynchronize() {
        lock = new Object();
    }

    public synchronized int getNextValueSynch() {
        return value++;
    }

    public int getNextValue() {
        synchronized (lock) {
            return value++;
        }
    }

    @Override
    public int getCurrentValue() {
        return value;
    }


}
