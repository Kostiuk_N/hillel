package textAnalyzer;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by Nikita_K on 24.05.2017.
 */
public class TextAnalyzer {

    public static WordsHolder analyze(String pathToFile) throws IOException {
        Path path = Paths.get(pathToFile);
        return analyze(path);
    }

    public static WordsHolder analyze(Path path) throws IOException {
        if (Files.exists(path, LinkOption.NOFOLLOW_LINKS) && !Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
            WordsHolder words = new WordsHolder();
            Files.lines(path, StandardCharsets.UTF_8).flatMap(TextAnalyzer::toWords).filter(w -> !w.isEmpty()).map(String::toLowerCase).forEach(words::put);
            return words;
        } else {
            throw new RuntimeException("Please, check the path to file: " + path.toString());
        }
    }

    private static Stream<String> toWords(String line) {
        String[] words = line.trim().split("[\\s\\d,\\-\".\\\\();:$~{}%!&/\\[\\]]");
        return Arrays.stream(words);
    }


}
