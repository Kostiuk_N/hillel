package textAnalyzer;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created by Nikita_K on 24.05.2017.
 */
public class WordsHolder {

    private Map<String, Integer> holder;

    public WordsHolder() {
        holder = new TreeMap<>();
    }

    public void put(String key) {
        Integer value = Optional.ofNullable(holder.get(key)).orElse(0);
        holder.put(key, ++value);
    }


    public Map<String, Integer> getUnmodifiableHolder() {
        return Collections.unmodifiableMap(holder);
    }


    @Override
    public String toString() {
        return holder.entrySet().stream().map(entry -> entry.getKey() + " : " + entry.getValue() + ";")
                .collect(Collectors.joining("\n"));
    }
}
