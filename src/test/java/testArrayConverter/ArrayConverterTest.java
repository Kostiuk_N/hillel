package testArrayConverter;

import arrayConverter.ArrayConverter;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Nikita_K on 25.05.2017.
 */
public class ArrayConverterTest extends Assert {

    private static String[] basicArray = {"one, two, three"};

    @Test
    public void testConverting() {
        Collection<String> testCollection = new ArrayList<>(3);
        ArrayConverter.convertToCollection(basicArray, testCollection);
        assertArrayEquals(basicArray, testCollection.toArray());
    }


}
