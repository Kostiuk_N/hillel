package testIncrement;

import increment.AtomicIncrementSynchronize;
import increment.IncrementSynchronize;
import increment.LockIncrementSynchronize;
import increment.SychMethodIncrementSynchronize;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

/**
 * Created by Nikita_K on 25.05.2017.
 */
public class IncrementTest extends Assert {

    private static int threads        = 10;
    private static int loopsByThread  = 100;
    private static int expectedResult = 4000;


    private IncrementSynchronize[] incrementSynchronizes;

    @Before
    public void prepareIncrements() {
        IncrementSynchronize atomicInc = new AtomicIncrementSynchronize();
        IncrementSynchronize lockInc = new LockIncrementSynchronize();
        IncrementSynchronize synchMethodInc = new SychMethodIncrementSynchronize();
        IncrementSynchronize synchObjInc = new SychMethodIncrementSynchronize();
        incrementSynchronizes = new IncrementSynchronize[]{atomicInc, lockInc, synchMethodInc, synchObjInc};
    }

    @Test
    public void testAsyncIncrement() {
        CountDownLatch countDownLatch = new CountDownLatch(threads);

        List<Worker> workers = new ArrayList<>();
        for (int i = 0; i < threads; i++) {
            workers.add(new Worker(loopsByThread, countDownLatch, incrementSynchronizes));
            countDownLatch.countDown();
        }

        try {
            Executors.newFixedThreadPool(threads, r -> {
                Thread thread = new Thread(r);
                thread.setDaemon(true);
                return thread;
            }).invokeAll(workers);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int sum = Stream.of(incrementSynchronizes).mapToInt(IncrementSynchronize::getCurrentValue).sum();
        assertEquals(expectedResult, sum);
    }


    private static class Worker implements Callable<Void> {

        final int loops;
        IncrementSynchronize[] increments;
        CountDownLatch         countDownLatch;

        Worker(int loops, CountDownLatch countDownLatch, IncrementSynchronize... increments) {
            this.increments = increments;
            this.loops = loops;
            this.countDownLatch = countDownLatch;
        }

        @Override
        public Void call() {
            try {
                countDownLatch.await();
                System.out.println("Current thread: " + Thread.currentThread());
                for (int i = 0; i < loops; i++) {
                    for (IncrementSynchronize increment : increments) {
                        increment.getNextValue();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
