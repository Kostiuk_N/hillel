package testTextAnalyzer;

import textAnalyzer.TextAnalyzer;
import textAnalyzer.WordsHolder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Nikita_K on 24.05.2017.
 */
public class TestAnalyzerExample {

    private static final String PATH_TO_FILE = "sun.txt";

    public static void main(String[] args) throws IOException, URISyntaxException {
        Path path = Paths.get(Thread.currentThread().getContextClassLoader().getResource(PATH_TO_FILE).toURI());
        WordsHolder words = TextAnalyzer.analyze(path);
        System.out.println(words);
    }
}
