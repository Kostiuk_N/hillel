package testTextAnalyzer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import textAnalyzer.TextAnalyzer;
import textAnalyzer.WordsHolder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Nikita_K on 25.05.2017.
 */
public class TextAnalyzerTest extends Assert {

    private static final String PATH_TO_FILE = "moon.txt";
    private static Map<String, Integer> expectedWordsMap;

    @Before
    public void initMap() {
//        "The $only [Moon] {is} -an \"astronomical\" !only! body [that] is the%body& (only) 123; orbits:
        expectedWordsMap = new TreeMap<>();
        expectedWordsMap.put("the", 2);
        expectedWordsMap.put("moon", 1);
        expectedWordsMap.put("is", 2);
        expectedWordsMap.put("an", 1);
        expectedWordsMap.put("astronomical", 1);
        expectedWordsMap.put("body", 2);
        expectedWordsMap.put("that", 1);
        expectedWordsMap.put("orbits", 1);
        expectedWordsMap.put("only", 3);
    }

    @Test
    public void testTextAnalyze() {
        try {
            Path path = Paths.get(Thread.currentThread().getContextClassLoader().getResource(PATH_TO_FILE).toURI());
            WordsHolder words = TextAnalyzer.analyze(path);
            assertEquals(expectedWordsMap, words.getUnmodifiableHolder());
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
    }


}
